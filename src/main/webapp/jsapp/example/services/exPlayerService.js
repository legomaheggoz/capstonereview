exampleApp.factory('exPlayerService', function($resource) {
	
	var playersRes = $resource('/capstone-review/app/players');
	
	return {
		getPlayers: getPlayers
	};
	
	function getPlayers() {
		return playersRes.query();
	}
	
	
});