pokedexApp.factory('findPokemonService', function($resource) {
		
	return {
		getPokemon: getPokemonById
	};
	
	function getPokemonById(Id) {
        var mapping = '/capstone-review/pokedexApp/pokemon/';
        var fullMapping = mapping + Id;
	    var pokemonRes = $resource(fullMapping);
		return pokemonRes.query();
	}
    
	
});