pokedexApp.factory('pokemonService', function($resource) {
	
	var pokemonRes = $resource('/capstone-review/pokedexApp/pokemon');
	
	return {
		getPokemon: getPokemon
	};
	
	function getPokemon() {
		return pokemonRes.query();
	}
    
	
});