"use strict"

var pokedexApp = angular.module('pokedex', ['ngRoute', 'ngResource']);

pokedexApp.config([$routeProvider, function($routeProvider){
	$routeProvider.when('/index',
			{
				templateUrl: '/capstone-review/jsapp/pokedex/templates/index.tpl.html',
				controller: 'PokedexCtrl',
                resolve: {
					pokemon:function(pokemonService){
						return pokemonService.getPokemon();
					}
            }
				
			
    $routeProvider.when('/profile/:Id',
            {
                templateUrl: 'capstone-review/jsapp/pokedex/templates/pokemonProfile.tpl.html',
                controller: 'profileCtrl',
                resolve: {
					players:function(findPokemonService){
						return findPokemonService.getPokemonById(Id);
					}
        
            }
                        
    $routeProvider.when('/edit',
            {
                templateUrl: 'capstone-review/jsapp/pokedex/templates/edit.tpl.html',
                controller: 'editCtrl',
            }
    $routeProvider.otherwise({redirectTo: "index"})
    
}]);