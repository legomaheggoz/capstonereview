package com.metlife.dpa.capstonereview.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.metlife.dpa.capstonereview.model.Pokemon;

@Repository
public interface PokemonRepository extends MongoRepository<Pokemon, String> {

}
