package com.metlife.dpa.capstonereview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.metlife.dpa.capstonereview.model.Pokemon;
import com.metlife.dpa.capstonereview.repository.PokemonRepository;

@RestController
public class ExampleRestController {

	@Autowired
	PokemonRepository pokemonRepository;
	
	@RequestMapping("/pokemon")
	public List<Pokemon> pokemons(){
		return pokemonRepository.findAll();
	}
	
	@RequestMapping(value="/seedpokemons", method=RequestMethod.POST)
	public void seedPokemons(){
		List<Pokemon> pokemons = new ArrayList<Pokemon>();
		pokemons.add(new Pokemon("Bulbasaur", "Grass", "Fire", "Ivysaur"));
		//pokemons.add(new Pokemon("Ivysaur", "Grass", "Fire", "Venosaur"));
		//pokemons.add(new Pokemon("Venosaur", "Grass", "Fire", "MAX"));
		//pokemons.add(new Pokemon("Squirtle", "Water", "Grass", "Wartortle"));
		//pokemons.add(new Pokemon("Wartortle", "Water", "Grass", "Blastoise"));
		//pokemons.add(new Pokemon("Blastoise", "Water", "Grass", "MAX"));
		//pokemons.add(new Pokemon("Charmander", "Fire", "Water", "Charmeleon"));
		//pokemons.add(new Pokemon("Charmeleon", "Fire", "Water", "Charizard"));
		//pokemons.add(new Pokemon("Charizard", "Fire", "Water", "MAX"));
		pokemonRepository.save(pokemons);
	}
}
