package com.metlife.dpa.capstonereview.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="pokemon")
public class Pokemon {
	
	@Id private String id;
	private String name;
	private String type;
	private String weakness;
	private String eInto;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public String getWeakness() {
		return weakness;
	}
	
	public String getEInto() {
		return eInto;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setWeakness(String weakness) {
		this.weakness = weakness;
	}
	
	public void setEInto(String eInto) {
		this.eInto = eInto;
	}
	
	public Pokemon(){}
	
	public Pokemon(String name, String type, String weakness, String eInto){
		this.name = name;
		this.type = type;
		this.weakness = weakness;
		this.eInto = eInto;
	}
}
